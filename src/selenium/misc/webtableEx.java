package selenium.misc;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class webtableEx {

	/**
	 * @param args -- 1st col 3 rows
	 * //*[@id="customers"]/tbody/tr[2]/td[1]
	 * //*[@id="customers"]/tbody/tr[3]/td[1]
	 * //*[@id="customers"]/tbody/tr[4]/td[1]
	 * 
	 * //*[@id="customers"]/tbody/tr[2]/td[2]
	 * //*[@id="customers"]/tbody/tr[3]/td[2]
	 * //*[@id="customers"]/tbody/tr[4]/td[2]
	 * 
	 * //*[@id="customers"]/tbody/tr[2]/td[3]
	 * //*[@id="customers"]/tbody/tr[3]/td[3]
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.w3schools.com/html/html_tables.asp");
		
		WebElement table = driver.findElement(By.id("customers"));
		
		List<WebElement> tr=table.findElements(By.tagName("tr"));
		int rows= tr.size();
		
		System.out.println("No of rows : "+rows);
		
		List<WebElement> td=	tr.get(1).findElements(By.tagName("td"));
		
		int cols= td.size();
		
		System.out.println("No of columns : "+ cols);
		
		
		
		String xpath_start="//*[@id='customers']/tbody/tr[";
		String xpath_mid="]/td[";
		String xpath_end="]";
		
		
		for(int i=2;i<=rows;i++){// rows
			for(int j=1;j<=cols;j++){ //cols
				String final_xpath=xpath_start+i+xpath_mid+j+xpath_end;
				WebElement elem= driver.findElement(By.xpath(final_xpath));
				System.out.print(elem.getText()+"  ||  ");
			}
			System.out.println();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
