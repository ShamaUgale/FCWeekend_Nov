package selenium.alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ok {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.javascriptkit.com/javatutors/alert2.shtml");
		
		WebElement btn=driver.findElement(By.name("B2"));
		btn.click();
		
		Thread.sleep(2000);
		Alert alt= driver.switchTo().alert();
		
		
		String msg= alt.getText();
		
		System.out.println("Message is : " + msg);
		
		alt.accept();// click on OK/Yes/Allow
		
	}

}
