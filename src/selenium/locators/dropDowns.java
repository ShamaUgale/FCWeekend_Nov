package selenium.locators;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class dropDowns {


	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.commonfloor.com/agent/login?show_signup=1");
		
		WebElement cityDD=driver.findElement(By.id("city"));
		
		Select sec= new Select(cityDD);
		
		// use case 1- select an element into the dd
		sec.selectByIndex(67);
		Thread.sleep(2000);
		
		// this is the value attribute for the option tag under ur dd
		sec.selectByValue("Anand");
		Thread.sleep(2000);
		
		sec.selectByVisibleText("Pune");
		
		// use case 2 - fecth wats selected in the dd
		String selectedOption=sec.getFirstSelectedOption().getText();
		System.out.println("Selected option is : "+ selectedOption);
		
		// use case 3 - count the no of elelemnts in the dd
		List<WebElement> cities=sec.getOptions();
		System.out.println("No of cities : " + cities.size());
		
		
		// use case 4 - fetch all the elements from the dd
		Iterator<WebElement> it= cities.iterator();
		while(it.hasNext()){
			System.out.println(it.next().getText());
		}
		
		
		
	}

}
