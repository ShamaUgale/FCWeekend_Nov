package selenium.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class links {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		// way 1
		driver.get("http://www.flipkart.com");
		
		WebElement adLink=driver.findElement(By.linkText("Advertise"));
		adLink.click();
		
		driver.navigate().back();
		
		WebElement downloadAppLink= driver.findElement(By.partialLinkText("App"));
		downloadAppLink.click();
		
	}

}
