package selenium.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ex {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.w3schools.com/html/tryit.asp?filename=tryhtml_default");
		
		// switch to the frame where my elem lies
		
		//1. using index
		
		int noOfFrames=driver.findElements(By.tagName("iframe")).size();
		System.out.println("The no of frames on the page : " + noOfFrames);
//		driver.switchTo().frame(1);
		
		
		// 2. using id or name given to the frame
//		driver.switchTo().frame("iframeResult");
		
		
		WebElement frame= driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		
		driver.switchTo().frame(frame);
		
		
		
		WebElement headingText=driver.findElement(By.xpath("/html/body/h1"));
		String txt= headingText.getText();
		
		System.out.println("The text is : "+ txt);
		
		
		driver.switchTo().defaultContent();
		
	}

}
