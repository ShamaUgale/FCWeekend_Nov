package selenium.waits;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class explicitwaits {

	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");

		WebDriverWait wait20= new WebDriverWait(driver, 20);
		wait20.until(ExpectedConditions.elementToBeClickable(By.id("xyz")));
		wait20.until(ExpectedConditions.elementSelectionStateToBe(By.id("x"), true));
		
	}
}
