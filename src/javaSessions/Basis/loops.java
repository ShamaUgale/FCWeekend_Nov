package javaSessions.Basis;

public class loops {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		// print all num which r multiples of 5 from 1-100
		
		//1. init of the counter
		// cond
		//stop - chnage the scope/state of the counter ++ --
		
		// while
		//do while
		//for
		
		/* while(cond){
			// set of stmt
		  }
		
		*/
		
		int i=1;
		while(i<=100){
			if(i%5==0){
				System.out.println(i);
			}
			i++;
		}
		
		
		System.out.println("***********************************");
		
		int j=995;
		
		do{
			if(j%5==0){
				System.out.println(j);
			}
			j++;
		}while(j<=100);
		
		
		
		
		System.out.println("*******************************************");
		
		// print num 1-10
		for(int k=1;k<=10;k++){
			System.out.println(k);
		}
		
		
		// print if the  num is divisible by 2 and 4 from 1-100

		
	}

}
