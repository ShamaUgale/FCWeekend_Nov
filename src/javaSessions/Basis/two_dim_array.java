package javaSessions.Basis;

public class two_dim_array {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		
		int[][] arr= new int[5][4]; //[rows][cols]
		
		arr[0][0]=78;
		arr[0][1]=34;
		arr[0][2]=87;
		arr[0][3]=56;
		
		
		arr[1][0]=97;
		arr[1][1]=458;
		arr[1][2]=12;
		arr[1][3]=23;
		
		arr[2][0]=54;
		arr[2][1]=19;
		arr[2][2]=78;
		arr[2][3]=88;
		
		
		arr[3][0]=40;
		arr[3][1]=18;
		arr[3][2]=98;
		arr[3][3]=75;
		
		arr[4][0]=70;
		arr[4][1]=73;
		arr[4][2]=63;
		arr[4][3]=83;
		
		System.out.println(arr[3][1]);
		System.out.println("No of rows : " + arr.length);
		System.out.println("No of cols : " + arr[0].length);
		
		int rows= arr.length;
		int cols=arr[0].length;
		
		for(int i=0;i<rows ; i++){//0,1,2,3,4
			for(int j=0;j<cols;j++){//0123
				System.out.print(arr[i][j]+" ");//00,01,02,03
			}
			System.out.println();
		}
			
		
		
	}

}
