package javaSessions.Basis;

public class continue_keyword {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// continue skips the current iteration and takes the loop to the next iteration
		
		for(int i=1;i<=10;i++){
			int mul= 2*i;
			
			if(mul==10){
				continue;
			}
			System.out.println("2*"+ i + "="+ mul);
		}
		
	}

}
