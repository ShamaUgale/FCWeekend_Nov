package javaSessions.Basis;

public class one_dim_array {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		
		int a=90;
		a=91;
		// arrays are fixed sized data types
		int[] arr= new int[4];
		arr[0]=6;
		arr[1]=56;
		arr[2]=78;
		arr[3]=9;
		
		System.out.println(arr[2]);
		System.out.println("No of elements in an array are : "+arr.length);
		
		
		
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
		
		
	}

}
