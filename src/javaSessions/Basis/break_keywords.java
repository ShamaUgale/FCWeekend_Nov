package javaSessions.Basis;

public class break_keywords {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		// break - will terminate the loop at that iteration
		// print the multiplication table for 2 till it reaches 10
		
		for(int i=1;i<=10;i++){
			int mul= 2*i;
			if(mul>10){
				break;
			}
			System.out.println("2*"+ i + "="+ mul);
		}
	}

}
