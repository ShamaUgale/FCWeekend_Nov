package javaSessions.Basis;

public class operators {

	public static void main(String[] args) {

		
		System.out.println("arithmatic ops");
		int i=99;
		int j=99;
		
		// +   -   *   /   %
		
		System.out.println("i+j = " + (i+j));
		System.out.println("i-j = " + (i-j));
		System.out.println("i*j = " + (i*j));
		System.out.println("i/j = " + (i/j));
		System.out.println("i%j = " + (i%j));
		
		// return u boolean
		System.out.println("comparison / conditional operators ");
		// >     <     >=      <=     ==     !=
		
		System.out.println("i<j = " + (i<j)); // is i less than j?
		System.out.println("i>j = " + (i>j));
		System.out.println("i>=j = " + (i>=j));
		System.out.println("i<=j = " + (i<=j));
		System.out.println("i==j = " + (i==j));
		System.out.println("i!=j = " + (i!=j));
		
		System.out.println("Logical ops");
		//AND &&  OR ||   NOT !
		
		int age=18;
		
		System.out.println(age>=18 && age<60);
		System.out.println(! (age<18 || age>=60) );
		
		
		System.out.println("Increment/decrement ops");
		// ++   post x++  ===> use x and then do x+1 ,,, pre ++x ====> do x+1 , then use x
		//--
		
		
		
		int x=8;
		
		System.out.println("post inc : " + (x++));
		System.out.println(x);
		
		
		x=8;
		System.out.println("pre inc : " + (++x));
		System.out.println(x);
		
		
		
		
		
		
		
		
		
		
		
	}

}
