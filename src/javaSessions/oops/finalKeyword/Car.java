package javaSessions.oops.finalKeyword;


// final class cannot be extended
public final class Car {

	
	String Mfr;
	String model;
	double price;
	int wheels;
	String color;
	
	
	// final methods cannot be override
	public final void start(){
		System.out.println("Car is starting");
	}
	
	
	public  void start(int a){
		System.out.println("Car is starting");
	}
	
	public void stop(){
		System.out.println("Car is stopping");
	}
	
	
	
}
