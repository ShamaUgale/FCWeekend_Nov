package javaSessions.oops.Strings;

public class Exx {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String result="Apple iPhone 6 (Space Grey, 64 GB)";
		
		String expectedString="iPhone";
		
		String welcomeMsg=" Welcome User123 ";
		
		String successMsg= "Your transaction was successfull, for future use 23344556667777 as your reference id.";
		
		String text="Hello";
		
		
		// compare 2 strings
		if(text.equals("hello")){
			System.out.println("Strings match");
		}else{
			System.out.println("Strings do not match");
		}
		
		if(text.equalsIgnoreCase("hello")){
			System.out.println("Strings match");
		}else{
			System.out.println("Strings do not match");
		}
		
		System.out.println(welcomeMsg.trim());
		
		String[] temp=successMsg.split("[ ]");
		System.out.println(temp[7]);
		
		System.out.println(successMsg.split("[ ]")[7]);
		
		System.out.println(welcomeMsg.trim().split("[ ]")[1]);
		
		System.out.println("The length of the string : " + welcomeMsg.length());
		
		System.out.println("Char at 10th index : "+ successMsg.charAt(10));
		
		System.out.println("Index of a is : "+ successMsg.indexOf('a'));
		
		System.out.println(welcomeMsg.contains("Wel"));
		
		
		
		
		
		
	}

}
