package javaSessions.oops.Constructors;

public class Car {

	
	String Mfr;
	String model;
	double price;
	int wheels;
	String color;
	
	public Car(String Mfr,String model,double price,int wheels,String color){
		this.color=color;
		this.Mfr=Mfr;
		this.model=model;
		this.price=price;
		this.wheels=wheels;
	}
	
	
	public void start(){
		System.out.println("Car is starting");
	}
	
	
	public void stop(){
		System.out.println("Car is stopping");
	}
	
	
	
}
