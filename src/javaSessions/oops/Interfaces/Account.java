package javaSessions.oops.Interfaces;

public interface Account {

	int minBalance=10000;
	
	 void openAccount();
	 void deposit();
	 void withdraw();
	 void closeAccount();
	
	
}
