package javaSessions.oops.classesAndObjects;

public class ex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		// classname var= new classname();
		
		Pen p= new Pen();
		System.out.println(p.Mfr);
		
		p.inkColor="Green";
		p.price=88.9;
		p.Mfr="Camlin";
		p.type="Marker";
		
		
		p.write();
		p.displayValues();
		
		
		System.out.println("****************************");
		Pen p1= new Pen();
//		System.out.println(p.Mfr);
		
		p1.inkColor="Black";
		p1.price=12.9;
		p1.Mfr="Camlin";
		p1.type="Gel Pen";
		
		
		p1.write();
		p1.displayValues();
		
		
		System.out.println("****************************");

		Car c= new Car();
		c.color="White";
		c.price=888888.9;
		c.Mfr="Honda";
		c.model="City";
		c.wheels=5;
		
		c.start();
		c.stop();
	
		
	}

}
