package javaSessions.oops.classesAndObjects;

public class Fan {
	
	int noOfBlades;
	String model;
	double price;
	
	
	public void on(){
		System.out.println("Putting the fan ON");
	}

	public void off(){
		System.out.println("Putting the fan OFF");
	}

}
