package javaSessions.oops.CollectionsAPIs;

import java.util.ArrayList;
import java.util.HashMap;

public class fbList {

	public static void main(String[] args) {

		HashMap <String , ArrayList<String>> FB= new HashMap <String , ArrayList<String> >();
		
		ArrayList<String> shama= new ArrayList<String>();
		shama.add("Santosh");
		shama.add("Reema");
		shama.add("Deepak");
		shama.add("Amit");
		shama.add("Anand");
		
		
		ArrayList<String> reema= new ArrayList<String>();
		reema.add("Santosh");
		reema.add("Shama");
		reema.add("Deepak");
		reema.add("Trisha");
		reema.add("Rekha");
		
		ArrayList<String> santosh= new ArrayList<String>();
		santosh.add("Reema");
		santosh.add("Shama");
		santosh.add("Deepak");
		santosh.add("Karthik");
		santosh.add("Rekha");
		
		FB.put("Shama", shama);
		FB.put("Reema", reema);
		FB.put("Santosh", santosh);
		
		System.out.println(FB);
		System.out.println(FB.get("Santosh"));
		
		
		
	}

}
