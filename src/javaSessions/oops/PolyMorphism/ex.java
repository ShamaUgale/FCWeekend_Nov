package javaSessions.oops.PolyMorphism;

public class ex {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		System.out.println("**************** overloading  *******************");
		Calculator c= new Calculator();
		c.add(55, 77);
		c.add(333, 456, 456);
		
		
		System.out.println("*************** overriding *****************");
		
		Car ca= new Car();
		ca.start();
		
		SUV s= new SUV();
		s.start();
		
	}

}
