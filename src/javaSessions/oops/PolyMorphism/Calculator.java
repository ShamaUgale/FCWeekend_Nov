package javaSessions.oops.PolyMorphism;

public class Calculator {

	
	
	
	public void add(){
		System.out.println("Add with no params");
	}
	
	public void add(int a){
		System.out.println("Add with 1 params : " + a);
	}
	
	public void add(int a, int b){
		System.out.println("a+b : " + (a+b));
	}
	
	public void add(String a, int b){
		System.out.println("a+b : " + (a+b));
	}
	
	
	public void add(int a, String b){
		System.out.println("a+b : " + (a+b));
	}
	
	/*  this is not polymorphimsm - not overloading - changing the return type is noy overloading
	public int add(int a, int b){
		System.out.println("a+b" + (a+b));
	}
	*/
	
	
	public void add(int a, int b, int c){
		System.out.println("a+b+c : " + (a+b+c));
	}
	
}
