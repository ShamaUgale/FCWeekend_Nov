package javaSessions.oops.Abstraction;

public abstract class Loan {

	double amount;
	String AccNo;
	
	public void apply(){
		System.out.println("Applying form Loan");
	}
	
	public abstract void submitDocs();
	
	public void disburse(){
		System.out.println("Disbursing the loan amount");
	}
	
	public abstract void calculateEMI();
	
	public void repay(){
		System.out.println("Making the payment towards the loan");
	}
	
	public void close(){
		System.out.println("Closing the loan.");
	}
	
	
	
	
	
	
	
}
