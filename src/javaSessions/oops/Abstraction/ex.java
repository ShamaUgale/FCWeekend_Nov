package javaSessions.oops.Abstraction;

public class ex {

	public static void main(String[] args) {

		HomeLoan h= new HomeLoan();
		h.apply();
		h.submitDocs();
		h.disburse();
		h.calculateEMI();
		h.repay();
		h.close();
		
		System.out.println();
		EduLoan e= new EduLoan();
		e.apply();
		e.submitDocs();
		e.disburse();
		e.calculateEMI();
		e.repay();
		e.close();
	}

}
