package testng.dependancy;


import org.testng.Assert;
import org.testng.annotations.Test;

public class UserCrud {

	@Test(groups={"smoke","regression"},description="Create user test",priority=1)
	public void createUser(){
		System.out.println("createUser");
		Assert.assertTrue(false);
	}
	
	@Test(groups={"regression"},description="Read user test",priority=2, dependsOnMethods="createUser")
	public void readUser(){
		System.out.println("readUser");
	}
	
	
	@Test(groups={"regression"},description="update user test",priority=3,dependsOnMethods="createUser")
	public void updateteUser(){
		System.out.println("updateUser");
	}
	
	
	@Test(groups={"smoke"},description="TC02456- delete user test",priority=4,dependsOnMethods="createUser")
	public void deleteUser(){
		System.out.println("deleteUser");
	}
	
	
	
}
