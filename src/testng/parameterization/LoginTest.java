package testng.parameterization;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTest {

	
	@Test(dataProvider="getLoginData", groups="smoke")
	public void loginTest(String username, String password, boolean isvalidSet){
		System.out.println("User : "+ username);
		System.out.println("Password : "+ password);
		
		boolean isOnDashBoard = true; // fetched from selenium
		
		
		if(isvalidSet){
			Assert.assertTrue(isOnDashBoard,"With invalid credentials user is logged in. And the credentials are : username=>"+ username+ " password=>"+password);
		}else{
			Assert.assertFalse(isOnDashBoard,"With invalid credentials user is logged in. And the credentials are : username=>"+ username+ " password=>"+password);
		}
		
	}
	 
	
	
	@DataProvider
	public Object[][] getLoginData(){
		Object[][] data= new Object[4][3];
		
		data[0][0]="U1";
		data[0][1]="P1";
		data[0][2]=true;
		
		data[1][0]="U2";
		data[1][1]="P2";
		data[1][2]=false;

		
		data[2][0]="U3";
		data[2][1]="P3";
		data[2][2]=true;

		
		data[3][0]="U4";
		data[3][1]="P4";
		data[3][2]=false;

		
		return data;
	}
	
	
	
	
	
}
