package testng.annotaions;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class A {
	
	
	
	// @Test
	//@BeforeTest
	//@BeforeSuite
	//@BeforeMethod
	
	
	@BeforeSuite
	public void beforeSuite(){
		// all prerequisite steps - which would run before every test
		
		System.out.println("beforeSuite");
	}
	
	@AfterMethod
	public void afterMethod(){
		// all prerequisite steps - which would run before every test
		
		System.out.println("AfterMethod");
	}
	
	
	
	@BeforeTest
	public void beforeTest(){
		// all prerequisite steps - which would run before every test
		
		System.out.println("BeforeTest");
	}
	
	
	@BeforeClass
	public void beforeClass(){
		// all prerequisite steps - which would run before every test
		
		System.out.println("BeforeClass - A");
	}
	
	@BeforeMethod
	public void beforeMethod(){
		// all prerequisite steps - which would run before every test
		
		System.out.println("BeforeMethod");
	}
	
	@Test(groups={"regression"})
	public void TC001(){
		// all the steps in selenium
		System.out.println("TC001");
	}
	
	
	@Test(groups={"regression"})
	public void TC002(){
		// all the steps in selenium
		System.out.println("TC002");
	}
	
	@Test(groups={"regression"})
	public void TC003(){
		// all the steps in selenium
		System.out.println("TC003");
	}
	
	

}
