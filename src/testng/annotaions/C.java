package testng.annotaions;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class C {

	
	@BeforeClass
	public void beforeClass(){
		// all prerequisite steps - which would run before every test
		
		System.out.println("BeforeClass - C");
	}
	
	@Test(groups={"smoke"})
	public void TC006(){
		// all the steps in selenium
		System.out.println("TC006");
	}
	
	
	@Test(groups={"regression"})
	public void TC007(){
		// all the steps in selenium
		System.out.println("TC007");
	}
	
	@Test(groups={"regression"})
	public void TC008(){
		// all the steps in selenium
		System.out.println("TC008");
	}
	
	
	@Test(groups={"regression"})
	public void TC009(){
		// all the steps in selenium
		System.out.println("TC009");
	}
	
}
