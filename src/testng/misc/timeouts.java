package testng.misc;

import org.apache.commons.io.FileExistsException;
import org.testng.annotations.Test;

public class timeouts {

	
	@Test(timeOut=3000)
	public void timeCheck() throws InterruptedException{
		Thread.sleep(4000);
	}
	
	@Test(expectedExceptions=InterruptedException.class)
	public void exceptionTests() throws FileExistsException {
		throw new FileExistsException();
	
	}
	
}
