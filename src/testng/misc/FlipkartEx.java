package testng.misc;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FlipkartEx {

	
	ChromeDriver driver=null;
	
	@BeforeClass
	public void setUP(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.get("http://www.flipkart.com");
	}
	
	@Test(dataProvider="getData")
	public void seearchProductTest(String productName){
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys(productName+Keys.ENTER);
	//	driver.findElement(By.xpath("//button[@class='vh79eN' and type='submit']")).click();
		
		WebElement searchedTxtFor=driver.findElement(By.xpath("//span[@class='W-gt5y']"));
		String actualProductName=searchedTxtFor.getText();
		
		Assert.assertTrue(searchedTxtFor.isDisplayed(),"Search results page was not displayed");
		
		Assert.assertEquals(actualProductName, productName,"The searche for and displayed text on search results page do not match");
		
	}
	
	
	@DataProvider
	public Object[][] getData(){
		Object[][] data= new Object[4][1];
		
		data[0][0]="Samsung galaxy";
		
		data[1][0]="Mi band 2";
		
		data[2][0]="Moto G 3";
	
		
		data[3][0]="iphone 6s";
	
		
		return data;
	}
	
	
	
	
}
